const jwt = require("jsonwebtoken");
const config = require("config");

//@middleware to authenticate if our user is valid
module.exports = (req, res, next) => {
  //@get user's token from header
  const token = req.header("x-auth-token");

  //if there isn't a token...
  if (!token) {
    return res.status(401).json({ msg: " sorry, you aren't allowed here!" });
  }

  //verify if the token contains valid secret
  try {
    const decoded = jwt.verify(token, config.get("jwtSecret")); //decode token
    //@set and add "user" to req obj to the token, we can then use this user in any of our protected routes to allow them to access these routes.
    req.user = decoded.user; //(giving req a user property with the user's data so it can be used later by w/e next is)
    next();
  } catch (err) {
    res.status(401).json({ msg: "no valid token" });
  }
};
