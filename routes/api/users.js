const express = require("express");
const router = express.Router();
const gravatar = require("gravatar");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require("config");
const User = require("../../models/User.js"); //user model mongoose
const { check, validationResult } = require("express-validator"); //validates if the user sends the right stuff

/* The following routes:
1) register a user w/ validation and JWT
*/

//1) route POST api/users
//This route is for REGISTERING USERS
//access public
router.post(
  "/",
  [
    check("name", "Name is required").not().isEmpty(),
    check("email", "Please include a valid email").isEmail(),
    check("password", "Enter a password with 6 or more characters").isLength({
      min: 6,
    }),
  ],
  async (req, res) => {
    const errors = validationResult(req); //validate that the request follows the rules
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { name, email, password } = req.body; //destructure req, DRY
    try {
      //@see if user exists
      let user = await User.findOne({ email: email }); //query our DB to search for email
      if (user) {
        res.status(400).json({ errors: [{ msg: "User already exists" }] });
      }

      //@if user does not exist, create new:
      const avatar = gravatar.url(email, {
        s: "200",
        r: "pg",
        d: "mm",
      }); //using gravatar to get users email thumbnail

      user = new User({
        name,
        email,
        avatar,
        password,
      }); //use User Model to create new model to add to DB

      //NOTE: these requests below are asynchronous, gotta await before moving on
      const salt = await bcrypt.genSalt(10); //salt to encrypt user's pw
      user.password = await bcrypt.hash(password, salt); //set user pw = hash of pw + salt
      await user.save(); //send request to save user to DB

      //@return jsonwebtoken to user so they can authenticate and access protected routes
      const payload = {
        user: {
          id: user.id,
        },
      };
      jwt.sign(
        payload,
        config.get("jwtSecret"), //pass in secret
        { expiresIn: 36000 },
        (err, token) => {
          if (err) throw err;
          res.json({ token });
        }
      );

      //@catch errors
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server Error");
    }
  }
);

module.exports = router;
