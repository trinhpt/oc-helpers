const { gql } = require("apollo-server-express");
// const gravatar = require("gravatar");
// const bcrypt = require("bcryptjs");
// const jwt = require("jsonwebtoken");
// const config = require("config");
// const User = require("../../models/User.js"); //user model mongoose
// const { check, validationResult } = require("express-validator"); //validates if the user

//---typeDefs, resolvers, move later to graphql schema---
const typeDefs = gql`
  #your schema will go here.
  type Query {
    hello: String!
  }

  type Mutation {
    #user mutation params: name, email, password
    registerUser(name: String!, email: String!, password: String!): String!
  }
`;

const resolvers = {
  Query: {
    hello: () => "hello from oc-helpers",
  },
  Mutation: {
    registerUser: async (_, { name, email, password }) => {
      return "user is registered";
    },
  },
};

module.exports = {
  typeDefs,
  resolvers,
};
