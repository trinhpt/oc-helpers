const express = require("express");
const { ApolloServer } = require("apollo-server-express");
const { typeDefs, resolvers } = require("./graphql/schema");
const connectDB = require("./config/db");
const path = require("path");

const app = express();
const PORT = process.env.PORT || 3000;

//graphQL
const server = new ApolloServer({
  typeDefs,
  resolvers,
  playground: true,
});
server.applyMiddleware({ app });

//db connection
connectDB();

//express built in body parser i.e req.body
app.use(express.json({ extended: false }));

//routes
app.use("/api/users", require("./routes/api/users"));
app.use("/api/auth", require("./routes/api/auth"));
app.use("/api/profile", require("./routes/api/profile"));
app.use("/api/posts", require("./routes/api/posts"));

if (process.env.NODE_ENV === "production") {
  app.use(express.static("client/build"));
  //404 error for non offcial routes

  app.get("john", function (req, res) {
    res.sendFile(path.resolve(__dirname, "client", "build", "index.html"));
  });
}

app.listen(PORT, () => {
  console.log("server started");
  console.log(`🚀 Server ready at http://localhost:3000${server.graphqlPath}`);
});
